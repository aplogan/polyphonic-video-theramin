from subprocess import check_output

def get_volume_control_name():
    try:
        results = check_output(["amixer", "scontrols"])
        # Get first single-quote-delimited guy
        return results.decode("utf-8").split("'")[1]
    except:
        pass
    return None

def get_curr_volume():
    try:
        ctrl_name = get_volume_control_name()
        results = check_output(["amixer", "sget", "-M", ctrl_name])
        for result in results.decode("utf-8").split():
            # Look for a square-bracketed percent
            if result[0] == '[' and result[-2:] == '%]':
                # Convert from string to number, then truncate to int
                return int(float(result[1:-2]))
    except:
        pass
    return 50

def get_volume_stats():
    # Current, minimum, maximum, increment
    return [get_curr_volume(), 0, 100, 5]

def set_volume(value):
    try:
        ctrl_name = get_volume_control_name()
        results = check_output( \
            ["amixer", "sset", "-M", ctrl_name, "{}%".format(value)])
        return True
    except:
        return False
