synths = [:square, :beep, :blade, :prophet, :growl]
live_loop :setup do
  n = sync "/osc*/synths"
  sname_a = synths[n[0]].to_sym()
  sname_b = synths[n[1]].to_sym()
  sname_c = synths[n[2]].to_sym()
  sname_d = synths[n[3]].to_sym()

  a = synth sname_a, note: 60, amp: 0, release: 1000, note_slide: 0.1
  set :asynth,a

  b = synth sname_b, note: 60, amp: 0, release: 1000, note_slide: 0.1
  set :bsynth,b

  c = synth sname_c, note: 60, amp: 0, release: 1000, note_slide: 0.1
  set :csynth,c

  d = synth sname_d, note: 60, amp: 0, release: 1000, note_slide: 0.1
  set :dsynth,d
end

live_loop :quit do
  m = sync "/osc*/quit"
  if (m[0] == 1)
    kill get(:asynth)
    kill get(:bsynth)
    kill get(:csynth)
    kill get(:dsynth)
  end
end

live_loop :ch1 do
  use_real_time
  # amp 0-1
  x_a, y_a, z_a = sync "/osc*/ch1"
  control get(:asynth), note: x_a if x_a > 0
  control get(:asynth), amp: y_a if y_a > 0
  control get(:asynth), cutoff: z_a if z_a > 0
end

live_loop :ch2 do
  use_real_time
  # amp 0-1
  x_b, y_b, z_b = sync "/osc*/ch2"
  control get(:bsynth), note: x_b if x_b > 0
  control get(:bsynth), amp: y_b if y_b > 0
  control get(:bsynth), cutoff: z_b if z_b > 0
end

live_loop :ch3 do
  use_real_time
  # amp 0-1
  x_c, y_c, z_c = sync "/osc*/ch3"
  control get(:csynth), note: x_c if x_c > 0
  control get(:csynth), amp: y_c if y_c > 0
  control get(:csynth), cutoff: z_c if z_c > 0
end

live_loop :ch4 do
  use_real_time
  # amp 0-1
  x_d, y_d, z_d = sync "/osc*/ch4"
  control get(:dsynth), note: x_d if x_d > 0
  control get(:dsynth), amp: y_d if y_d > 0
  control get(:dsynth), cutoff: z_d if z_d > 0
end
