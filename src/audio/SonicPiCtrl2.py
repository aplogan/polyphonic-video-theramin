#!/usr/bin/env python3
#send command to SP from input
#need to install pythonosc library
#use pip3 install pythonosc
#I saved the script as typeCommandOSC.py
from time import sleep
from pythonosc import udp_client
from pythonosc import osc_message_builder
import argparse
import sys

def control(spip):
    sender = udp_client.SimpleUDPClient(spip,4559)
    while True:
        try:
            instr = input('Enter input line: ')
            if len(instr)> 0:
              sender.send_message('/control',instr) #sends entered command
              print("/control",instr)
            else:
              print("null input")
            sleep(0.5)
        except KeyboardInterrupt:
            print("\nExiting")
            sys.exit()

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--sp",
    default="127.0.0.1", help="The ip Sonic Pi listens on")
    args = parser.parse_args()
    spip=args.sp
    print("Sonic Pi on ip",spip)
    sleep(2)
    control(spip)
