from pythonosc import udp_client
import enum

synths    = ['square', 'beep', 'blade', 'prophet', 'growl' ]
effects   = ['pitch', 'volume', 'cutoff']
voice_map = { synth : ['pitch', 'volume', 'cutoff'] for synth in synths }
no_effect = '-'

KEY_PITCH  = effects.index('pitch')
KEY_VOLUME = effects.index('volume')
KEY_CUTOFF = effects.index('cutoff')

class Key(enum.IntEnum):
    CHANNEL = enum.auto()
    X       = enum.auto()
    Y       = enum.auto()
    Z       = enum.auto()

class SonicPiInterface:
    __max_chan     = 4
    __max_pitch    = 120
    __max_volume   = 2
    __max_cutoff   = 130
    __init_pitch   = 0
    __init_volume  = 0
    __init_cutoff  = 0
    __dflt_pitch   = 60
    __dflt_volume  = 2
    __dflt_cutoff  = 130

    def __init__(self, server_ip="127.0.0.1", server_port=4560):
        self.client  = udp_client.SimpleUDPClient(server_ip, server_port)
        self.synths  = []
        self.effects = []
        self.map     = {}

    def setup_synth(self, hand_nums, synth, effects):
        assert synth in synths
        assert isinstance(hand_nums, list) and len(hand_nums) == 2
        assert isinstance(hand_nums[0], int)
        assert hand_nums[1] is None or isinstance(hand_nums[1], int)
        assert isinstance(effects, list)
        assert len(effects) in [3,6]
        assert hand_nums

        self.synths.append(synth)
        self.effects.append([self.__init_pitch, self.__init_volume, self.__init_cutoff])
        channel = len(self.synths)

        self.map[ hand_nums[0] ] = { Key.CHANNEL : channel, \
            Key.X : effects[0], Key.Y : effects[1], Key.Z : effects[2] }

        if not hand_nums[1] is None:
            self.map[ hand_nums[1] ] = { Key.CHANNEL : channel, \
            Key.X : effects[3], Key.Y : effects[4], Key.Z : effects[5] }

    def __set_default_effects(self):
        # Anything not under dynamic control should be set to a static,
        # nonzero default value. This is especially necessary for
        # volume, whose initial value is 0.
        for i in range(len(self.synths)):
            channel = i+1
            # Determine which hands are controlling this channel
            hnums = [hnum for hnum in self.map if self.map[hnum][Key.CHANNEL] == channel]
            # Look for each effect being controlled
            for effect in effects:
                found_it = False
                for hnum in hnums:
                    found_it = self.map[hnum][Key.X] == effect or \
                               self.map[hnum][Key.Y] == effect or \
                               self.map[hnum][Key.Z] == effect
                    if found_it:
                        break
                if not found_it:
                    if effect == 'pitch':
                        self.effects[channel-1][KEY_PITCH]  = self.__dflt_pitch
                    elif effect == 'volume':
                        self.effects[channel-1][KEY_VOLUME] = self.__dflt_volume
                    elif effect == 'cutoff':
                        self.effects[channel-1][KEY_CUTOFF] = self.__dflt_cutoff

    def start_play(self):
        synth_ids = [synths.index(synth) for synth in self.synths]
        # Sonic Pi script expects there to always be a fixed number of
        # synths. (Those that we don't care about/want will play
        # silently in the background.) Tell Sonic Pi which synths we
        # want to play.
        while len(synth_ids) < self.__max_chan:
            synth_ids.append(0)
        self.client.send_message("/synths", synth_ids)

        self.__set_default_effects()

        for i in range(len(self.synths)):
            self.__update_channel(i+1)

    def __update_channel(self, channel):
        self.client.send_message("/ch{}".format(channel), self.effects[channel-1])

    def __set_effect(self, channel, coord, effect):
        if effect == 'pitch':
            self.effects[channel-1][KEY_PITCH]  = self.__max_pitch  * (coord+1) / 2
        elif effect == 'volume':
            self.effects[channel-1][KEY_VOLUME] = self.__max_volume * (coord+1) / 2
        elif effect == 'cutoff':
            self.effects[channel-1][KEY_CUTOFF] = self.__max_cutoff * (coord+1) / 2

    def __update_effects(self, hand_num, x, y, z):
        if not hand_num in self.map:
            return 0
        channel = self.map[hand_num][Key.CHANNEL]
        self.__set_effect(channel, x, self.map[hand_num][Key.X])
        self.__set_effect(channel, y, self.map[hand_num][Key.Y])
        self.__set_effect(channel, z, self.map[hand_num][Key.Z])
        return channel

    def set_control(self, hand_num, x, y, z):
        self.__update_channel(self.__update_effects(hand_num, x, y, z))

    def stop_play(self):
        self.client.send_message("/quit", [1])
        self.synths.clear()
        self.effects.clear()
        self.map.clear()
