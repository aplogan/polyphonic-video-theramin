#! /usr/bin/env python3
import os, sys
sys.path.insert(1, os.path.abspath('gui'))
sys.path.insert(1, os.path.abspath('audio'))
sys.path.insert(1, os.path.abspath('video'))
sys.path.insert(1, os.path.abspath('misc'))
import gui
gui.main()
