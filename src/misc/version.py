import cv2
import pygame as pg

THEREMIN_VERSION = '1.0'
SONIC_PI_VERSION = '3.2.2'
OPEN_CV_VERSION  = cv2.__version__
PYGAME_VERSION   = '{}.{}.{}'.format(pg.vernum[0], pg.vernum[1], pg.vernum[2])
