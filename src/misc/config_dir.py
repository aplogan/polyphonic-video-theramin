from pathlib import Path

def get_path():
   return Path.home().joinpath('.vbp_theremin')

def ensure_exists():
    config_dir = get_path()
    if not config_dir.is_dir():
        config_dir.mkdir(mode=0o770, parents=True)
