import cv2 as cv
import copy

class MultiFrameDisplay:
    __padding = 10

    def __init__(self, window_names):
        self.tiled = False

        # imshow() can create named windows for us, but we'd be stuck
        # with the default properties.
        self.window_names = copy.deepcopy(window_names)
        for name in self.window_names:
            cv.namedWindow(name, cv.WINDOW_AUTOSIZE | cv.WINDOW_GUI_NORMAL)

    def tile_windows(self, frames):
        total_width = 0
        for i in range(len(self.window_names)):
            cv.moveWindow(self.window_names[i], \
                (i+1)*self.__padding + total_width, self.__padding)
            height, width, _ = frames[i].shape
            total_width += width
        self.tiled = True

    def display_frames(self, frames):
        if not self.tiled:
            self.tile_windows(frames)
        for i in range(len(self.window_names)):
            cv.imshow(self.window_names[i], frames[i])

    def cleanup(self):
        for name in self.window_names:
            cv.destroyWindow(name)
