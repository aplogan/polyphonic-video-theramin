import track_hands

def is_calibrated(num_hands):
    return track_hands.is_calibrated(num_hands)

def run_calibration(num_hands):
    track_hands.calibrate_to_file(num_hands)

def start_tracking(num_hands, conn_g2v_v, sonic_pi):
    track_hands.track_hands_from_cal_files( \
        num_hands, conn_g2v_v=conn_g2v_v, sonic_pi=sonic_pi)

def stop_tracking(v2g_conn):
    v2g_conn.send(track_hands.STOP_MESSAGE)
