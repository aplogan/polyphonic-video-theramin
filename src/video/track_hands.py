import cv2 as cv
import numpy as np
import config_dir
from pathlib import Path
from dual_webcam_video_stream import DualWebcamVideoStream as Capture
from multi_frame_display      import MultiFrameDisplay     as Display
import math

CAMERA_PATH_1      = './usbcam1'
CAMERA_PATH_2      = './usbcam2'
CAMERA_FPS         = None # Use default
WIN_NAME_1         = 'Left Camera'
WIN_NAME_2         = 'Right Camera'
STOP_MESSAGE       = "Stop tracking!"
TEXT_COLOR         = (255,0,255)
TEXT_THICKNESS     = 2
ANNOTATE_COLOR     = (255,0,255)
ANNOTATE_THICKNESS = 2
HIST_BOX_COLOR     = (0,255,0)
HIST_BOX_ROWS      = 2
HIST_BOX_COLS      = 2
HIST_BOX_HEIGHT    = 10
HIST_BOX_WIDTH     = 10
ENTER_KEY          = 13
EXIT_KEY           = 27

def hist_file_name(hand_num, cam_num):
    return config_dir.get_path().joinpath( \
        'hist_hand_{}_cam_{}.txt'.format(hand_num, cam_num))

def is_calibrated(num_hands):
    for i in range(num_hands):
        if not hist_file_name(i+1, 1).is_file():
            return False
        if not hist_file_name(i+1, 2).is_file():
            return False
    return True

def make_hist_boxes(frame):
    frame_rows, frame_cols, _ = frame.shape
    hist_boxes = []

    for i in range(HIST_BOX_ROWS):
        y = int( (3*i+8.5)*frame_rows/20 )
        for j in range(HIST_BOX_COLS):
            x = int( (j+9.5)*frame_cols/20 )
            hist_boxes.append([x, x+HIST_BOX_WIDTH, y, y+HIST_BOX_HEIGHT])

    return hist_boxes

def overlay_message(frame, message):
    cv.putText(frame, message, (10,35), \
        cv.FONT_HERSHEY_SIMPLEX, 0.8, TEXT_COLOR, TEXT_THICKNESS)

def overlay_hist_boxes(frame, hist_boxes):
    for i in range(len(hist_boxes)):
        cv.rectangle(frame, \
            (hist_boxes[i][0], hist_boxes[i][2]), \
            (hist_boxes[i][1], hist_boxes[i][3]), \
            HIST_BOX_COLOR, 1)

def make_hand_hist(frame, hist_boxes):
    hsv_frame = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    num_boxes = HIST_BOX_ROWS * HIST_BOX_COLS
    roi = np.zeros([num_boxes*HIST_BOX_HEIGHT, HIST_BOX_WIDTH, 3], \
        dtype=hsv_frame.dtype)

    for i in range(num_boxes):
        roi[i*HIST_BOX_HEIGHT:(i+1)*HIST_BOX_HEIGHT, 0:HIST_BOX_WIDTH] = \
            hsv_frame[hist_boxes[i][2]:hist_boxes[i][3],
                      hist_boxes[i][0]:hist_boxes[i][1]]

    hand_hist = cv.calcHist([roi], [0, 1], None, [180, 256], [0, 180, 0, 256])
    return cv.normalize(hand_hist, hand_hist, 0, 255, cv.NORM_MINMAX)

def hist_masking(frame, hist):
    hsv       = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    dst       = cv.calcBackProject([hsv], [0, 1], hist, [0, 180, 0, 256], 1)
    disc      = cv.getStructuringElement(cv.MORPH_ELLIPSE, (31, 31))
    cv.filter2D(dst, -1, disc, dst)
    _, thresh = cv.threshold(dst, 150, 255, cv.THRESH_BINARY)
    # thresh  = cv.dilate(thresh, None, iterations=5)
    thresh    = cv.merge((thresh, thresh, thresh))
    return cv.bitwise_and(frame, thresh)

def find_hand_contour(frame, hand_hist):
    masked    = hist_masking(frame, hand_hist)
    masked    = cv.erode(masked,  None, iterations=2)
    masked    = cv.dilate(masked, None, iterations=2)
    gray      = cv.cvtColor(masked, cv.COLOR_BGR2GRAY)
    _, thresh = cv.threshold(gray, 0, 255, 0)
    conts, _  = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    return max(conts, key=cv.contourArea) if len(conts) else None

def centroid(contour):
    moment = cv.moments(contour)
    if moment['m00'] != 0:
        cx = int(moment['m10'] / moment['m00'])
        cy = int(moment['m01'] / moment['m00'])
        return cx, cy
    else:
        return None

def annotate_frame(frame, hand_contour, hand_num):
    if hand_contour is None:
        return
    hand_centroid = centroid(hand_contour)
    cvx_hull      = cv.convexHull(hand_contour)
    cv.drawMarker(frame, hand_centroid,   ANNOTATE_COLOR, thickness=ANNOTATE_THICKNESS)
    cv.drawContours(frame, [cvx_hull], 0, ANNOTATE_COLOR, thickness=ANNOTATE_THICKNESS)
    cv.putText(frame, str(hand_num+1), (hand_centroid[0]+5,hand_centroid[1]+23), \
        cv.FONT_HERSHEY_SIMPLEX, 0.8, ANNOTATE_COLOR, ANNOTATE_THICKNESS)

def calibrate(num_hands, capture, display):
    hist_boxes = None
    hand_hists = [[None for j in range(num_hands)] for i in [0,1]]
    hand_num   = 0
    cam_num    = 0
    do_hist    = True

    while True:
        pressed_key = cv.waitKey(1) & 0xFF
        frames      = capture.read()
        frames[0]   = cv.flip(frames[0], 1)
        frames[1]   = cv.flip(frames[1], 1)

        if hist_boxes == None:
            hist_boxes = [make_hist_boxes(frames[i]) for i in [0,1]]

        if hand_num == num_hands:
            for i in [0,1]:
                overlay_message(frames[i], "Good overall tracking? (press Y or N)")
                for j in range(num_hands):
                    hand_contour = find_hand_contour(frames[i], hand_hists[i][j])
                    annotate_frame(frames[i], hand_contour, j)

            if pressed_key == ord('y'):
                # Done with calibration
                break

            elif pressed_key == ord('n'):
                # Start over from the first camera and first hand
                hand_num = 0
                cam_num  = 0
                do_hist  = True

        elif do_hist:
            if pressed_key == ENTER_KEY:
                hand_hists[cam_num][hand_num] = \
                    make_hand_hist(frames[cam_num], hist_boxes[cam_num])
                do_hist = False
            else:
                overlay_message(frames[cam_num], "Get histogram for hand #{} (press ENTER)".format(hand_num+1))
                overlay_hist_boxes(frames[cam_num], hist_boxes[cam_num])
        else:
            overlay_message(frames[cam_num], "Good tracking? (press Y or N)")
            hand_contour = find_hand_contour( \
                frames[cam_num], hand_hists[cam_num][hand_num])
            annotate_frame(frames[cam_num], hand_contour, hand_num)

            if pressed_key == ord('y'):
                # Proceed to calibration for next camera/hand combo
                cam_num = (cam_num + 1) % 2
                if cam_num == 0:
                    hand_num += 1
                do_hist = True

            elif pressed_key == ord('n'):
                # Redo calibration for current camera/hand combo
                do_hist = True

        display.display_frames(frames)

    return hand_hists

def compute_location(shape_0, shape_1, centroid_0, centroid_1):
    u_0 = 2*centroid_0[0]/shape_0[1] - 1
    u_1 = 2*centroid_1[0]/shape_1[1] - 1
    v_0 = 1 - 2*centroid_0[1]/shape_0[0]
    v_1 = 1 - 2*centroid_1[1]/shape_1[0]
    x = (u_0 + u_1) / 2
    y = (u_0 - u_1) / 2
    z = (v_0 + v_1) / 2
    x = math.erf(1.8*x)
    y = math.erf(1.8*y)
    z = math.erf(1.8*z)
    return x, y, z

def track_hands(hand_hists, capture, display=None, conn_g2v_v=None, sonic_pi=None):

    do_stdout = conn_g2v_v is None and sonic_pi is None

    while True:
        if not conn_g2v_v is None and conn_g2v_v.poll() and conn_g2v_v.recv() == STOP_MESSAGE:
            break

        pressed_key = cv.waitKey(1) & 0xFF
        frames = capture.read()
        frames[0] = cv.flip(frames[0], 1)
        frames[1] = cv.flip(frames[1], 1)

        for j in range(len(hand_hists[0])):
            contour_0   = find_hand_contour(frames[0], hand_hists[0][j])
            contour_1   = find_hand_contour(frames[1], hand_hists[1][j])
            centroid_0  = centroid(contour_0)
            centroid_1  = centroid(contour_1)
            if not (centroid_0 is None or centroid_1 is None):
                x, y, z = compute_location( \
                    frames[0].shape, frames[1].shape, \
                    centroid_0, centroid_1)
                if not sonic_pi is None:
                    sonic_pi.set_control(j+1,x,y,z)
                if not conn_g2v_v is None:
                    conn_g2v_v.send("({} {:.2f} {:.2f} {:.2f})".format(j+1,x,y,z))
                if do_stdout:
                    print("({} {:.2f} {:.2f} {:.2f})".format(j+1,x,y,z))

            if not display is None:
                annotate_frame(frames[0], contour_0, j)
                annotate_frame(frames[1], contour_1, j)

        if not display is None:
            display.display_frames(frames)

        if pressed_key == EXIT_KEY:
            break

def calibrate_to_file(num_hands, capture=None, display=None):
    own_capture = (capture is None)
    if own_capture:
        capture = Capture(src1=CAMERA_PATH_1, src2=CAMERA_PATH_2, fps=CAMERA_FPS).start()

    own_display = (display is None)
    if own_display:
        display = Display([WIN_NAME_1, WIN_NAME_2])

    hand_hists = calibrate(num_hands, capture, display)

    config_dir.ensure_exists()
    for i in [0,1]:
        for j in range(num_hands):
            file_name = hist_file_name(j+1, i+1)
            np.savetxt(file_name, hand_hists[i][j])

    if own_capture:
        capture.release()
    if own_display:
        display.cleanup()

def track_hands_from_cal_files(num_hands, capture=None, display=None, conn_g2v_v=None, sonic_pi=None):
    own_capture = (capture is None)
    if own_capture:
        capture = Capture(src1=CAMERA_PATH_1, src2=CAMERA_PATH_2, fps=CAMERA_FPS).start()

    hand_hists = [[None for j in range(num_hands)] for i in [0,1]]
    for i in [0,1]:
        for j in range(num_hands):
            file_name = hist_file_name(j+1, i+1)
            hand_hists[i][j] = np.loadtxt(file_name, dtype=np.float32)

    track_hands(hand_hists, capture, display, conn_g2v_v, sonic_pi)

    if own_capture:
        capture.release()

if __name__ == '__main__':
    capture = Capture(src1=CAMERA_PATH_1, src2=CAMERA_PATH_2, fps=CAMERA_FPS).start()
    display = Display([WIN_NAME_1, WIN_NAME_2])
    calibrate_to_file(2, capture, display)
    track_hands_from_cal_files(2, capture, display)
    capture.release()
    display.cleanup()
