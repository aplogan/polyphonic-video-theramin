# import the necessary packages
import cv2, threading

class DualWebcamVideoStream:
	def __init__(self, src1=0, src2=1, name="DualWebcamVideoStream", fps=None):
		if src1 == src2:
			raise ValueError(("Video stream sources ({} and {}) " +\
				" must be unique.").format(src1, src2))

		# initialize the video camera streams and read the first frames
		# from each
		self.stream1 = cv2.VideoCapture(src1)
		self.stream2 = cv2.VideoCapture(src2)
		
		if fps is not None:
			self.stream1.set(cv2.CAP_PROP_FPS, fps)
			self.stream2.set(cv2.CAP_PROP_FPS, fps)
		
		(self.grabbed1, self.frame1) = self.stream1.read()
		(self.grabbed2, self.frame2) = self.stream2.read()

		# initialize the thread name
		self.name = name

		# initialize the variable used to indicate if the thread should
		# be stopped
		self.stopped = False
		
		# lock to synchronize access to self.stopped
		self.lock = threading.Lock()

	def start(self):
		# start the thread to read frames from the video stream
		t = threading.Thread(target=self.update, name=self.name, args=())
		t.daemon = True
		t.start()
		return self

	def update(self):
		# keep looping infinitely until the thread is stopped
		while True:
			# if the thread indicator variable is set, stop the thread
			with self.lock:
				if self.stopped:
					return

			# otherwise, read the next frames from the streams

			# From the OpenCV documentation for cv.VideoCapture.grab():
			#     The primary use of the function is in multi-camera
			#     environments, especially when the cameras do not have
			#     hardware synchronization. That is, you call
			#     VideoCapture::grab() for each camera and after that
			#     call the slower method VideoCapture::retrieve() to
			#     decode and get frame from each camera. This way the
			#     overhead on demosaicing or motion jpeg decompression
			#     etc. is eliminated and the retrieved frames from
			#     different cameras will be closer in time.
			self.grabbed1  = self.stream1.grab()
			self.grabbed2  = self.stream2.grab()
			_, self.frame1 = self.stream1.retrieve()
			_, self.frame2 = self.stream2.retrieve()

	def read(self):
		# return the frames most recently read
		return [self.frame1, self.frame2]

	def stop(self):
		# indicate that the thread should be stopped
		with self.lock:
			self.stopped = True

	def release(self):
		self.stop()
		# close video capturing devices
		self.stream1.release()
		self.stream2.release()
