import pygame as pg
import button, style, util
from color import darken

class ScrollOption:
    __border_color = (127,127,127) # Medium gray
    __bg_color     = (255,255,255) # White
    __fg_color     = ( 24, 24, 24) # Dark gray

    def __init__(self, rect, option_init, options):
        self.visible         = False
        self.callback        = None
        self.callback_arg    = None
        btn_width            = round(0.7*rect.height)
        btn_height           = rect.height
        self.max_text_width  = rect.width - 2*btn_width - 2*style.padding
        self.max_text_height = rect.height - 2


        self.left = button.Button( \
            pg.Rect(rect.left, rect.top, btn_width, btn_height), \
            self.__bg_color, text=style.ctrl_text_left, \
            text_color=self.__fg_color, text_font=style.font_symb)

        self.right = button.Button( \
            pg.Rect(rect.left+rect.width-btn_width, rect.top, btn_width, btn_height), \
            self.__bg_color, text=style.ctrl_text_right, \
            text_color=self.__fg_color, text_font=style.font_symb)

        self.rect = rect.copy()
        shrink = style.width_border // 2
        self.rect.left   += shrink
        self.rect.top    += shrink
        self.rect.width  -= 2*shrink
        self.rect.height -= 2*shrink

        self.set_opts(option_init, options)

    def cleanup(self):
        pass

    def __update_label(self, shift=0):
        self.index = (self.index + shift) % len(self.options)
        self.label = self.font.render(self.options[self.index], True, self.__fg_color)
        self.label_rect = self.label.get_rect(center=self.rect.center)

    def set_scroll_callback(self, callback, arg=None):
        self.callback = callback
        self.callback_arg = arg

    def register_event(self, event):
        if not self.visible:
            return
        self.left.register_event(event)
        self.right.register_event(event)
        if self.left.is_activated():
            self.__update_label(-1)
            if self.callback != None:
                self.callback(self.callback_arg)
        elif self.right.is_activated():
            self.__update_label(+1)
            if self.callback != None:
                self.callback(self.callback_arg)

    def set_visible(self, visible):
        self.visible = visible

    def get_visible(self):
        return self.visible

    def draw_on(self, surface):
        if not self.visible:
            return
        pg.draw.rect(surface, self.__bg_color, self.rect)
        pg.draw.rect(surface, self.__border_color, self.rect, 1)
        surface.blit(self.label, self.label_rect)
        self.left.draw_on(surface)
        self.right.draw_on(surface)

    def get_opt(self):
        return self.options[self.index]

    def set_opt(self, option):
        try:
            self.index = self.options.index(option)
            self.__update_label()
        except ValueError:
            raise ValueError("Option '{}' not found in list of possible options.".format(option))

    def set_opts(self, option_init, options):
        self.options = options

        longest_opt = ""
        for option in options:
            if len(option) > len(longest_opt):
                longest_opt = option

        self.font, _ = util.get_fitted_font(style.font_text, \
            longest_opt, [self.max_text_width, self.max_text_height])

        self.set_opt(option_init)
