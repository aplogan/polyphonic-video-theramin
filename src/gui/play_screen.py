import pygame as pg
import multiprocessing as mp
import button, color, enum, math, sonic_pi_interface, style, util, video_interface, voice_display

class Flavor(enum.IntEnum):
    H1V1      = enum.auto()
    H2V2      = enum.auto()
    H2V1      = enum.auto()
    H3V3      = enum.auto()
    H4V4      = enum.auto()
    H4V2      = enum.auto()

class PlayScreen:
    __back_color  = color.matlab['red']
    __play_color  = color.matlab['green']
    __stop_color  = color.matlab['red']
    __cal_color   = color.matlab['purple']
    __disp_colors = [ color.matlab['blue'  ], color.matlab['cyan'  ], \
                      color.matlab['orange'], color.matlab['yellow'] ]

    def __init__(self, rect, flavor):
        self.visible    = False
        self.rect       = rect
        self.buttons    = {}
        self.play       = False
        self.displays   = []
        self.is_playing = False
        self.sonic_pi   = sonic_pi_interface.SonicPiInterface()
        ctrl_btn_width  = math.floor(1.5 * style.ctrl_btn_height)

        self.buttons['back'] = button.Button( \
            pg.Rect(rect.left, rect.top, ctrl_btn_width, style.ctrl_btn_height), \
            self.__back_color, text=style.ctrl_text_back, \
            text_color=style.color_text, text_font=style.font_symb)

        self.buttons['play_stop'] = button.Button( \
            pg.Rect(rect.left + ctrl_btn_width, rect.top, \
                    rect.width - 2*ctrl_btn_width, style.ctrl_btn_height), \
            self.__play_color, text=style.ctrl_text_play, \
            text_color=style.color_text, text_font=style.font_symb)

        self.buttons['calibrate'] = button.Button( \
            pg.Rect(rect.right - ctrl_btn_width, rect.top, ctrl_btn_width, style.ctrl_btn_height), \
            self.__cal_color, text=style.ctrl_text_cal, \
            text_color=style.color_text, text_font=style.font_symb)

        if flavor == Flavor.H1V1:
            self.num_hands, self.hands_per_voice, rows, cols = 1, 1, 1, 1
        elif flavor == Flavor.H2V2:
            self.num_hands, self.hands_per_voice, rows, cols = 2, 1, 1, 2
        elif flavor == Flavor.H2V1:
            self.num_hands, self.hands_per_voice, rows, cols = 2, 2, 1, 1
        elif flavor == Flavor.H3V3:
            self.num_hands, self.hands_per_voice, rows, cols = 3, 1, 2, 2
        elif flavor == Flavor.H4V4:
            self.num_hands, self.hands_per_voice, rows, cols = 4, 1, 2, 2
        elif flavor == Flavor.H4V2:
            self.num_hands, self.hands_per_voice, rows, cols = 4, 2, 2, 1

        rects = util.make_rect_grid(pg.Rect(self.rect.left, \
            self.rect.top + style.ctrl_btn_height, self.rect.width, \
            self.rect.height - style.ctrl_btn_height), rows, cols)

        voice_num = 1
        hand_num = 1
        for i in range(rows):
            for j in range(cols):
                if hand_num > self.num_hands:
                    break
                self.displays.append(voice_display.VoiceDisplay( \
                    rects[i*cols + j], self.rect, \
                    self.__disp_colors[voice_num-1], voice_num, \
                    hand_num, \
                    (hand_num + 1 if self.hands_per_voice == 2 else None)))
                voice_num += 1
                hand_num  += self.hands_per_voice

        self.__set_play_button_enable()

    def cleanup(self):
        for disp in self.displays:
            disp.cleanup()

    def __start_play(self):
        for disp in self.displays:
            self.sonic_pi.setup_synth( \
                disp.get_hand_nums(), \
                disp.get_setup().get_voice(), \
                disp.get_setup().get_effects())

        self.sonic_pi.start_play()

        # Duplex pipe: GUI <--> video system
        self.conn_g2v_v, self.conn_g2v_g = mp.Pipe(True)

        self.video_process = mp.Process(target=video_interface.start_tracking, \
            args=(self.num_hands, self.conn_g2v_v, self.sonic_pi))

        self.video_process.start()

        self.is_playing = True

    def __stop_play(self):
        if self.is_playing:
            video_interface.stop_tracking(self.conn_g2v_g)
            self.video_process.join()
            self.sonic_pi.stop_play()

            # Clear coordinate readouts
            for disp in self.displays:
                disp.set_xyz(0,0,0, 1)
                if self.hands_per_voice:
                    disp.set_xyz(0,0,0, 2)

            self.is_playing = False

    def __toggle_play(self):
        self.play = not self.play
        self.buttons['back'     ].set_enabled(not self.play)
        self.buttons['calibrate'].set_enabled(not self.play)
        for disp in self.displays:
            disp.set_enabled(not self.play)
        if self.play:
            self.buttons['play_stop'].set_text(style.ctrl_text_stop)
            self.buttons['play_stop'].set_color(self.__stop_color)
            self.__start_play()
        else:
            self.buttons['play_stop'].set_text(style.ctrl_text_play)
            self.buttons['play_stop'].set_color(self.__play_color)
            self.__stop_play()

    def __set_play_button_enable(self):
        self.buttons['play_stop'].set_enabled( \
            video_interface.is_calibrated(self.num_hands))

    def register_event(self, event):
        if not self.visible:
            return
        for _, btn in self.buttons.items():
            btn.register_event(event)
        for disp in self.displays:
            disp.register_event(event)
        if self.buttons['play_stop'].is_activated():
            self.__toggle_play()

    def set_visible(self, visible):
        self.visible = visible
        for disp in self.displays:
            disp.set_visible(visible)

    def get_visible(self):
        return self.visible

    def __handle_video_messages(self):
        while self.conn_g2v_g.poll():
            msg = self.conn_g2v_g.recv()
            # Remove parens and split on spaces
            tokens   = msg[1:-1].split()
            hand_num = int(tokens[0]) - 1
            x, y, z  = [float(t) for t in tokens[1:]]
            disp_num = hand_num // self.hands_per_voice
            set_num  = hand_num % self.hands_per_voice + 1
            self.displays[disp_num].set_xyz(x, y, z, set_num)

    def draw_on(self, surface):
        if not self.visible:
            return
        if self.is_playing:
            self.__handle_video_messages()
        for _, btn in self.buttons.items():
            btn.draw_on(surface)
        for disp in self.displays:
            disp.draw_on(surface)

    def done(self):
        return self.visible and self.buttons['back'].is_activated()

    def calibrate_requested(self):
        return self.visible and self.buttons['calibrate'].is_activated()

    def calibrate(self):
        pg.display.quit()
        video_interface.run_calibration(self.num_hands)
        self.__set_play_button_enable()

    def get_displays(self):
        return self.displays

    def get_activated_display(self):
        for disp in self.displays:
            if disp.is_activated():
                return disp
        return None
