import pygame as pg
import button, color, enum, play_screen, style, util

class MainMenu:
    __text_height = 16

    def __init__(self, rect):
        self.visible = False
        self.buttons = {}

        rects = util.make_rect_grid(pg.Rect(rect.left, rect.top, \
            rect.width, rect.height-style.ctrl_btn_height), 3, 2)

        self.buttons[play_screen.Flavor.H1V1] = button.Button( \
            rects[0], color.matlab['red'], text='1 hand, 1 voice', \
            text_font=style.font_text, text_height=self.__text_height, text_color=style.color_text)

        self.buttons[play_screen.Flavor.H2V2] = button.Button( \
            rects[1], color.matlab['orange'], text='2 hands, 2 voices', \
            text_font=style.font_text, text_height=self.__text_height, text_color=style.color_text)

        self.buttons[play_screen.Flavor.H2V1] = button.Button( \
            rects[2], color.matlab['yellow'], text='2 hands, 1 voice', \
            text_font=style.font_text, text_height=self.__text_height, text_color=style.color_text)

        self.buttons[play_screen.Flavor.H3V3] = button.Button( \
            rects[3], color.matlab['green'], text='3 hands, 3 voices', \
            text_font=style.font_text, text_height=self.__text_height, text_color=style.color_text)

        self.buttons[play_screen.Flavor.H4V4] = button.Button( \
            rects[4], color.matlab['cyan'], text='4 hands, 4 voices', \
            text_font=style.font_text, text_height=self.__text_height, text_color=style.color_text)

        self.buttons[play_screen.Flavor.H4V2] = button.Button( \
            rects[5], color.matlab['blue'], text='4 hands, 2 voices', \
            text_font=style.font_text, text_height=self.__text_height, text_color=style.color_text)

        self.buttons['about'] = button.Button( \
            pg.Rect(rect.left, rects[5].bottom, rect.width, style.ctrl_btn_height), \
            color.matlab['purple'], text='About', \
            text_font=style.font_text, text_height=self.__text_height, text_color=style.color_text)

    def cleanup(self):
        pass

    def register_event(self, event):
        if not self.visible:
            return
        for _, btn in self.buttons.items():
            btn.register_event(event)

    def get_activated(self):
        activated = []
        if self.visible:
            for id, button in self.buttons.items():
                if button.is_activated():
                    activated.append(id)
        return activated

    def set_visible(self, visible):
        self.visible = visible

    def get_visible(self):
        return self.visible

    def draw_on(self, surface):
        if not self.visible:
            return
        for _, btn in self.buttons.items():
            btn.draw_on(surface)
