import pygame as pg
import RPi.GPIO as gpio

class ScreenShot:
    def __init__(self, \
                 trigger_chan, \
                 trigger_debounce_ms=300, \
                 file_name_stem='screen_dump_', \
                 file_name_ext='png', \
                 verbose=False):
        self.trigger_chan   = trigger_chan
        self.verbose        = verbose
        self.file_name_stem = file_name_stem
        self.file_name_ext  = file_name_ext
        self.triggered      = False
        self.save_count     = 0

        gpio.setmode(gpio.BCM)
        gpio.setup(trigger_chan, gpio.IN, pull_up_down=gpio.PUD_UP)
        gpio.add_event_detect(trigger_chan, gpio.FALLING, \
            callback=lambda x: self.__trigger(), \
            bouncetime=trigger_debounce_ms)

    def __trigger(self):
        self.triggered = True

    def cleanup(self):
        gpio.cleanup(self.trigger_chan)

    def take_screen_shot_if_triggered(self, screen, verbose=False):
        if not self.triggered:
            return
        self.triggered = False
        img_name = "{}{:02d}.{}".format( \
            self.file_name_stem, self.save_count, self.file_name_ext)
        pg.image.save(screen, img_name)
        if self.verbose or verbose:
            print("Wrote {}.".format(img_name))
        self.save_count += 1
