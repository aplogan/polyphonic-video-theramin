import pygame as pg
import RPi.GPIO as gpio
import gpio_assignments, math, style, time, volume_control

class VolumePopup:
    __timestamp_reset = -math.inf
    __timeout         = 5
    __width           = 80
    __height          = 220
    __sw_debounce_ms  = 300
    __color_bg        = (17,85,204)
    __color_outline   = (28,69,135)
    __icon_file       = 'misc/sound_icon.png'

    def __init__(self, rect):
        volume_stats     = volume_control.get_volume_stats()
        self.visible     = False
        self.rect        = pg.Rect(0, 0, self.__width, self.__height)
        self.rect.center = rect.center
        self.down_chan   = gpio_assignments.VOLUME_DOWN
        self.up_chan     = gpio_assignments.VOLUME_UP
        self.time_stamp  = self.__timestamp_reset
        self.volume      = volume_stats[0]
        self.vol_min     = volume_stats[1]
        self.vol_max     = volume_stats[2]
        self.vol_incr    = volume_stats[3]
        i_temp           = pg.image.load(self.__icon_file)
        i_w_max          = (self.rect.width - 3*style.padding) // 2
        i_h_max          = self.rect.height - 2*style.padding
        i_size           = i_temp.get_size()
        i_ratio          = min(i_w_max / i_size[0], i_h_max / i_size[1])
        i_width          = math.floor(i_ratio * i_size[0])
        i_height         = math.floor(i_ratio * i_size[1])
        self.icon        = pg.transform.smoothscale(i_temp, (i_width, i_height))
        self.icon_rect   = pg.Rect(self.rect.left + style.padding + (i_w_max - i_width) // 2, \
                                   self.rect.centery - i_height//2, \
                                   i_width, i_height)
        self.graph_rect  = pg.Rect(self.rect.left + 2*style.padding + i_w_max, \
                                   self.rect.top  +   style.padding,
                                   i_w_max, self.rect.height - 2*style.padding)
        self.fill_rect   = pg.Rect(self.graph_rect.left   + style.width_bar_outline, \
                                   self.graph_rect.top    + style.width_bar_outline, \
                                   self.graph_rect.width  - 2*style.width_bar_outline, \
                                   self.graph_rect.height - 2*style.width_bar_outline)

        gpio.setmode(gpio.BCM)
        gpio.setup([self.up_chan, self.down_chan], \
            gpio.IN, pull_up_down=gpio.PUD_UP)
        gpio.add_event_detect(self.down_chan, gpio.FALLING, \
            callback=lambda x: self.lower_volume(), \
            bouncetime=self.__sw_debounce_ms);
        gpio.add_event_detect(self.up_chan, gpio.FALLING, \
            callback=lambda x: self.raise_volume(), \
            bouncetime=self.__sw_debounce_ms);

    def lower_volume(self):
        if not self.visible:
            return
        new_volume = max(self.volume - self.vol_incr, self.vol_min)
        if volume_control.set_volume(new_volume):
            self.volume = new_volume
        self.time_stamp = time.time() + self.__timeout
        return self.volume

    def raise_volume(self):
        if not self.visible:
            return
        new_volume = min(self.volume + self.vol_incr, self.vol_max)
        if volume_control.set_volume(new_volume):
            self.volume = new_volume
        self.time_stamp = time.time() + self.__timeout
        return self.volume

    def cleanup(self):
        gpio.cleanup([self.down_chan, self.up_chan])

    def register_event(self, event):
        if not self.visible:
            return

    def set_visible(self, visible):
        self.visible = visible
        if not visible:
            self.time_stamp = self.__timestamp_reset

    def get_visible(self):
        return self.visible

    def draw_on(self, surface):
        if not self.visible or time.time() > self.time_stamp:
            return
        ratio = (self.volume  - self.vol_min) / \
                (self.vol_max - self.vol_min)
        fill = round(ratio * self.fill_rect.height)
        fill_rect = pg.Rect(self.fill_rect.left, \
                            self.fill_rect.bottom - fill, \
                            self.fill_rect.width, fill)
        pg.draw.rect(surface, self.__color_bg,         self.rect)
        pg.draw.rect(surface, self.__color_outline,    self.rect,       style.width_border)
        pg.draw.rect(surface, style.color_screen,      self.graph_rect)
        pg.draw.rect(surface, style.color_bar_outline, self.graph_rect, style.width_bar_outline)
        pg.draw.rect(surface, style.color_bar_fill,    fill_rect)
        surface.blit(self.icon, self.icon_rect)
