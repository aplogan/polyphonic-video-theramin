import pygame as pg
import style

def make_rect_grid(rect, num_rows, num_cols, pad_outer=0, pad_inner=0):
    if num_rows < 1 or not isinstance(num_rows, int):
        raise ValueError("Number of rows must be positive integer: {}".format(num_rows))
    if num_cols < 1 or not isinstance(num_cols, int):
        raise ValueError("Number of columns must be positive integer: {}".format(num_cols))

    rects = []
    if num_rows*num_cols == 1:
        rects.append(rect)
        return rects

    c_width  = (rect.width  - 2*pad_outer - (num_cols-1)*pad_inner) / num_cols
    r_height = (rect.height - 2*pad_outer - (num_rows-1)*pad_inner) / num_rows

    for i in range(num_rows):
        top    = rect.top + pad_outer + round(  i  *r_height + i*pad_inner)
        bottom = rect.top + pad_outer + round((i+1)*r_height + i*pad_inner)
        for j in range(num_cols):
                left  = rect.left + pad_outer + round(  j  *c_width + j*pad_inner)
                right = rect.left + pad_outer + round((j+1)*c_width + j*pad_inner)
                rects.append(pg.Rect(left, top, right - left, bottom - top))
    return rects

def get_fitted_font(font_face, test_string, max_size):
    font = None
    font_height = max_size[1]
    while True:
        font = pg.font.SysFont(font_face, font_height, \
            bold=(font_height < style.font_height_min_nonbold))
        size = font.size(test_string)
        if (size[0] <= max_size[0] and size[1] <= max_size[1]) \
            or font_height == style.font_height_min:
            break
        font_height -= 1
    return font, font_height
