import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = 'hide'
import pygame as pg
import about_screen, \
       bailout, \
       enum, \
       gpio_assignments, \
       main_menu, \
       play_screen, \
       screen_shot, \
       style, \
       time, \
       volume_popup

class state(enum.IntEnum):
    INIT   = enum.auto()
    SPLASH = enum.auto()
    MAIN   = enum.auto()
    ABOUT  = enum.auto()
    PLAY   = enum.auto()
    SETUP  = enum.auto()

class Gui:
    __splash_timeout = 5

    def __init__(self, rect):
        self.state  = [state.INIT]
        self.about  = about_screen.AboutScreen(rect)
        self.main   = main_menu.MainMenu(rect)
        self.play   = self.play = { flavor : play_screen.PlayScreen(rect, flavor) for flavor in play_screen.Flavor }
        self.volume = volume_popup.VolumePopup(rect)

        # volume *MUST* be last in the list so that it will be drawn on
        # top of other widgets
        self.children = []
        self.children.append(self.about)
        self.children.append(self.main)
        for flavor in self.play:
            self.children.append(self.play[flavor])
            for disp in self.play[flavor].get_displays():
                self.children.append(disp.get_setup())
        self.children.append(self.volume)

        self.last_play_visible      = None
        self.last_activated_display = None
        self.last_setup_visible     = None

    def cleanup(self):
        for child in self.children:
            child.cleanup()

    def register_event(self, event):
        for child in self.children:
            child.register_event(event)

    def update(self):
        if self.state[-1] == state.INIT:
            self.about.set_visible(True)
            self.splash_expiry = time.time() + self.__splash_timeout
            self.state[-1] = state.SPLASH

        elif self.state[-1] == state.SPLASH:
            if time.time() > self.splash_expiry:
                self.about.set_visible(False)
                self.main.set_visible(True)
                self.volume.set_visible(True)
                self.state[-1] = state.MAIN

        elif self.state[-1] == state.MAIN:
            # Get just the first menu item activated (if any); ignore the rest
            activated = self.main.get_activated()
            activated = activated[0] if len(activated) else None

            if isinstance(activated, play_screen.Flavor) and activated in play_screen.Flavor:
                self.main.set_visible(False)
                self.play[activated].set_visible(True)
                self.last_play_visible = self.play[activated]
                self.state.append(state.PLAY)

            elif activated == 'about':
                self.main.set_visible(False)
                self.volume.set_visible(False)
                self.about.set_visible(True, done_button=True)
                self.state.append(state.ABOUT)

        elif self.state[-1] == state.PLAY:
            if self.last_play_visible.done():
                self.last_play_visible.set_visible(False)
                self.main.set_visible(True)
                self.state.pop()

            elif self.last_play_visible.calibrate_requested():
                self.last_play_visible.calibrate()

            else:
                self.last_activated_display = \
                    self.last_play_visible.get_activated_display()
                if self.last_activated_display != None:
                    self.last_setup_visible = self.last_activated_display.get_setup()
                    if self.last_setup_visible != None:
                        self.last_play_visible.set_visible(False)
                        self.last_setup_visible.set_visible(True)
                        self.state.append(state.SETUP)

        elif self.state[-1] == state.SETUP:
            canceled = self.last_setup_visible.canceled()
            done     = self.last_setup_visible.done()
            if canceled or done:
                if done:
                    self.last_activated_display.apply_configs()
                self.last_setup_visible.set_visible(False)
                self.last_play_visible.set_visible(True)
                self.state.pop()

        elif self.state[-1] == state.ABOUT:
            if self.about.done():
                self.about.set_visible(False)
                self.main.set_visible(True)
                self.volume.set_visible(True)
                self.state.pop()

    def draw_on(self, surface):
        for child in self.children:
            child.draw_on(surface)

def conditional_putenv(var, val):
    if var not in os.environ:
        os.putenv(var, val)

def make_screen():
    pg.display.init()
    if 'ECE_5725_DEMO' in os.environ:
        pg.display.set_caption('Polyphonic Theremin')
        return pg.display.set_mode((320,240))
    else:
        pg.mouse.set_visible(False)
        return pg.display.set_mode()

def graphics_loop():
    screen    = make_screen()
    fps_clock = pg.time.Clock()
    loop_mgr  = bailout.Bailout(btn_chans=[gpio_assignments.QUIT])
    GUI       = Gui(screen.get_rect())
    imager    = screen_shot.ScreenShot(gpio_assignments.SAVE)

    while loop_mgr.is_running():
        for event in pg.event.get():
            if event.type == pg.QUIT:
                loop_mgr.set_running(False)
            else:
                GUI.register_event(event)

        GUI.update()

        # GUI.update() is permitted to uninitialize the pygame display
        # to perform GUI operations off the PiTFT. Those operations are
        # "atomic" with respect to a single call to GUI.update(). So
        # thereafter, resurrect the display screen.
        if not pg.display.get_init():
            screen = make_screen()

        screen.fill(style.color_screen)
        GUI.draw_on(screen)
        pg.display.flip()
        imager.take_screen_shot_if_triggered(screen)
        fps_clock.tick(style.frame_rate)

    loop_mgr.cleanup()
    GUI.cleanup()
    imager.cleanup()

def main():
    if 'ECE_5725_DEMO' not in os.environ:
        conditional_putenv('SDL_VIDEODRIVER', 'fbcon')
        conditional_putenv('SDL_FBDEV',       '/dev/fb1')
        conditional_putenv('SDL_MOUSEDRV',    'TSLIB')
        conditional_putenv('SDL_MOUSEDEV',    '/dev/input/touchscreen')
    pg.init()
    try:
        graphics_loop()
    except KeyboardInterrupt:
        print()
    pg.quit()

if __name__ == '__main__':
    main()
