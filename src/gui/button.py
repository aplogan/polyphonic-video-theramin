# Author: Aaron Logan (apl86)
# Date: 8 October 2020

import pygame as pg
import enum, math, style
from color import darken

class Shape(enum.Enum):
    RECTANGLE = 0
    ELLIPSE   = 1

class Button:
    __defaults = { \
            'color_active' : None,
            'shape'        : Shape.RECTANGLE, \
            'text'         : None, \
            'text_color'   : style.color_screen, \
            'text_font'    : None, \
            'text_height'  : None, \
            'text_bold'    : False, \
            'text_italic'  : False, \
            'enabled'      : True }

    def __init__(self, rect, color, **kwargs):
        # Populate missing arguments with defaults from above
        for prop, value in self.__defaults.items():
            if prop not in kwargs:
                kwargs[prop] = value

        # Special default values that depend on inputs
        if kwargs['text_height'] == None:
            kwargs['text_height'] = \
                max(style.font_height_min, rect.height - 2*style.padding)

        # Shrink rectangle to accomodate border
        shrink = style.width_border // 2
        rect.left   += shrink
        rect.top    += shrink
        rect.width  -= 2*shrink
        rect.height -= 2*shrink

        self.rect              = rect
        self.shape             = kwargs['shape']
        self.last_btn_down_pos = None
        self.active            = False
        self.activated         = False
        self.enabled           = kwargs['enabled']
        self.colors_enabled    = [color, kwargs['color_active']]
        self.colors_disabled   = [style.color_disabled, None]
        self.text_color        = kwargs['text_color']
        self.font              = pg.font.SysFont(kwargs['text_font'], \
                                                 kwargs['text_height'], \
                                                 bold=kwargs['text_bold'], \
                                                 italic=kwargs['text_italic'])
        self.set_text(kwargs['text'])
        self.__apply_colors()

    def set_text(self, text):
        if text == None:
            self.label, self.label_rect = None, None
        else:
            self.label = self.font.render(text, True, self.text_color)
            self.label_rect = self.label.get_rect( \
                    center=(self.rect.centerx, self.rect.centery))

    def __apply_colors(self):
        if self.enabled:
            self.color, self.color_active = self.colors_enabled
        else:
            self.color, self.color_active = self.colors_disabled
        if self.color_active == None:
            self.color_active = darken(self.color)
        self.bord_color = darken(self.color)
        self.bord_color_active = darken(self.color_active)

    def set_color(self, color, color_active=None):
        self.colors_enabled = [color, color_active]
        self.__apply_colors()

    def draw_on(self, surface):
        color = self.color_active if self.active else self.color
        bord_color = self.bord_color_active if self.active else self.bord_color
        if self.shape == Shape.RECTANGLE:
            pg.draw.rect(surface, color, self.rect)
            pg.draw.rect(surface, bord_color, self.rect, style.width_border)
        else:
            pg.draw.ellipse(surface, color, self.rect)
            pg.draw.ellipse(surface, bord_color, self.rect, style.width_border)
        if self.label != None:
            surface.blit(self.label, self.label_rect)

    def register_event(self, event):
        if not self.enabled:
            return
        if event.type == pg.MOUSEBUTTONDOWN:
            self.last_btn_down_pos = event.pos
            if self.rect.collidepoint(event.pos):
                self.active = True
            self.activated = False

        elif event.type == pg.MOUSEBUTTONUP:
            self.activated = self.rect.collidepoint(self.last_btn_down_pos) \
                         and self.rect.collidepoint(event.pos)
            self.last_btn_down_pos = None
            self.active = False


    def is_activated(self):
        activated = self.activated
        self.activated = False
        return activated

    def set_enabled(self, enabled):
        self.enabled           = enabled
        self.activated         = False
        self.active            = False
        self.last_btn_down_pos = None
        self.__apply_colors()
