import pygame as pg
import coord_display, style, util, voice_setup
from color import darken

class VoiceDisplay:
     # This is purposefully long to ensure text
     # size is chosen to fit within widgets
    __voice_init = 'Jordan Rudess Snarling Pig'

    def __init__(self, \
                 rect, \
                 setup_rect, \
                 color, \
                 voice_num, \
                 hand_num_1, \
                 hand_num_2=None, \
                 enabled=True):
        # Shrink rectangle to accomodate border
        shrink = style.width_border // 2
        rect.left   += shrink
        rect.top    += shrink
        rect.width  -= 2*shrink
        rect.height -= 2*shrink

        self.visible           = False
        self.rect              = rect
        self.last_btn_down_pos = None
        self.active            = False
        self.activated         = False
        self.enabled           = enabled
        self.colors_enabled    = [color, None]
        self.colors_disabled   = [style.color_disabled, None]
        self.voice_num         = voice_num
        self.hand_nums         = [hand_num_1, hand_num_2]
        self.two_hands         = hand_num_2 != None
        self.setup             = voice_setup.VoiceSetup(setup_rect, color, voice_num, hand_num_1, hand_num_2)
        self.displays          = []
        self.__apply_colors()

        pad_inner = round(0.025 * rect.height)

        rects = util.make_rect_grid(rect, 5, (2 if self.two_hands else 1), \
            style.padding, pad_inner)

        if self.two_hands:
            self.voice_rect = rects[0].union(rects[1])
        else:
            self.voice_rect = rects[0]

        self.font, _ = util.get_fitted_font( \
            style.font_text, self.__voice_init, \
            [self.voice_rect.width, self.voice_rect.height+6])
        w_xyz = max(self.font.size('X')[0], \
                    self.font.size('Y')[0], \
                    self.font.size('Z')[0])

        if self.two_hands:
            self.voice_rect = rects[0].union(rects[1])
        else:
            self.voice_rect = rects[0]

        self.label1 = self.font.render("Hand {}".format(hand_num_1), True, style.color_text)
        self.label1_rect = rects[2 if self.two_hands else 1]

        self.displays.append(coord_display.CoordDisplay('X', w_xyz, -1, 1, 0, self.font, rects[4 if self.two_hands else 2]))
        self.displays.append(coord_display.CoordDisplay('Y', w_xyz, -1, 1, 0, self.font, rects[6 if self.two_hands else 3]))
        self.displays.append(coord_display.CoordDisplay('Z', w_xyz, -1, 1, 0, self.font, rects[8 if self.two_hands else 4]))

        if self.two_hands:
            self.label2 = self.font.render("Hand {}".format(hand_num_2), True, style.color_text)
            self.label2_rect = rects[3]
            self.displays.append(coord_display.CoordDisplay('X', w_xyz, -1, 1, 0, self.font, rects[5]))
            self.displays.append(coord_display.CoordDisplay('Y', w_xyz, -1, 1, 0, self.font, rects[7]))
            self.displays.append(coord_display.CoordDisplay('Z', w_xyz, -1, 1, 0, self.font, rects[9]))

        self.apply_configs()

    def __apply_colors(self):
        if self.enabled:
            self.color, self.color_active = self.colors_enabled
        else:
            self.color, self.color_active = self.colors_disabled
        if self.color_active == None:
            self.color_active = darken(self.color)
        self.bord_color = darken(self.color)
        self.bord_color_active = darken(self.color_active)

    def set_color(self, color, color_active=None):
        self.colors_enabled = [color, color_active]
        self.__apply_colors()

    def cleanup(self):
        for disp in self.displays:
            disp.cleanup()

    def register_event(self, event):
        if not self.visible or not self.enabled:
            return
        if event.type == pg.MOUSEBUTTONDOWN:
            self.last_btn_down_pos = event.pos
            if self.rect.collidepoint(event.pos):
                self.active = True
            self.activated = False

        elif event.type == pg.MOUSEBUTTONUP:
            self.activated = self.rect.collidepoint(self.last_btn_down_pos) \
                         and self.rect.collidepoint(event.pos)
            self.last_btn_down_pos = None
            self.active = False

    def set_visible(self, visible):
        self.visible = visible
        for disp in self.displays:
            disp.set_visible(visible)

    def get_visible(self):
        return self.visible

    def __clamp_coord(self, coord):
        return max(self.__coord_min, min(self.__coord_max, coord))

    def set_xyz(self, x, y, z, set_num=1):
        if set_num == 1:
            self.displays[0].set(x)
            self.displays[1].set(y)
            self.displays[2].set(z)
        elif set_num == 2 and self.two_hands:
            self.displays[3].set(x)
            self.displays[4].set(y)
            self.displays[5].set(z)

    def draw_on(self, surface):
        if not self.visible:
            return
        if self.active:
            color, bord_color = self.color_active, self.bord_color_active
        else:
            color, bord_color = self.color, self.bord_color
        pg.draw.rect(surface, color, self.rect)
        pg.draw.rect(surface, bord_color, self.rect, style.width_border)
        surface.blit(self.voice,  self.voice_rect)
        surface.blit(self.label1, self.label1_rect)
        if self.two_hands:
            surface.blit(self.label2, self.label2_rect)
        for disp in self.displays:
            disp.draw_on(surface)

    def is_activated(self):
        activated = self.activated
        self.activated = False
        return activated

    def set_enabled(self, enabled):
        self.enabled           = enabled
        self.activated         = False
        self.active            = False
        self.last_btn_down_pos = None
        self.__apply_colors()

    def get_setup(self):
        return self.setup

    def get_hand_nums(self):
        return self.hand_nums

    def apply_configs(self):
        self.voice = self.font.render( \
            self.setup.get_voice(), True, style.color_text)
