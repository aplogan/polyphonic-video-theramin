import pygame as pg
import button, math, style, version

class AboutScreen:
    __version       = '0.0'
    __bg_color      = (179,27,27)   # Carnelian: official Cornell red
    __done_bg_color = (255,255,255) # White
    __done_fg_color = (24,24,24) # Dark gray

    def __init__(self, rect):
        self.rect         = rect
        self.visible      = False
        self.draw_done    = False
        self.strings      = []
        self.string_rects = []
        button_rect = pg.Rect(0, 0, math.floor(1.5 * style.ctrl_btn_height), \
                              style.ctrl_btn_height)
        button_rect.centerx = rect.centerx
        button_rect.bottom = rect.bottom - style.padding
        self.done_button = button.Button(button_rect,
            self.__done_bg_color, text=style.ctrl_text_done, \
            text_color=self.__done_fg_color, text_font=style.font_symb)

        self.add_string(26, "Video-Based Polyphonic")
        self.add_string(40, "Theremin", bold=True, italic=True)
        self.add_string(20, "Version " + version.THEREMIN_VERSION)
        self.add_string(16, "© 2020, sr2322 & apl86")
        self.add_string(16, "ECE 5725, Cornell University")
        self.add_string(14, "Built on Pygame {}, OpenCV {}, and Sonic Pi {}".format( \
            version.PYGAME_VERSION, version.OPEN_CV_VERSION, version.SONIC_PI_VERSION))

    def add_string(self, height, text, bold=False, italic=False):
        font   = pg.font.SysFont(style.font_text, height, bold, italic)
        string = font.render(text, True, style.color_text)
        rect   = string.get_rect()
        rect.centerx = self.rect.centerx
        if len(self.string_rects):
            rect.top = self.string_rects[-1].bottom
        else:
            rect.top = self.rect.top + style.padding

        self.strings.append(string)
        self.string_rects.append(rect)

    def cleanup(self):
        pass

    def register_event(self, event):
        if not self.visible:
            return
        if self.draw_done:
            self.done_button.register_event(event)

    def set_visible(self, visible, done_button=False):
        self.visible   = visible
        self.draw_done = visible and done_button

    def get_visible(self):
        return self.visible

    def draw_on(self, surface):
        if not self.visible:
            return
        y_shift = 0
        if not self.draw_done:
            y_shift = (self.rect.bottom - self.string_rects[-1].bottom) // 2
        pg.draw.rect(surface, self.__bg_color, self.rect)
        for i in range(len(self.strings)):
            surface.blit(self.strings[i], self.string_rects[i].move(0, y_shift))
        if self.draw_done:
            self.done_button.draw_on(surface)

    def done(self):
        return self.visible and self.draw_done and \
            self.done_button.is_activated()
