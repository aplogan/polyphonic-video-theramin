import pygame as pg
import button, math, scroll_option, style, util
import sonic_pi_interface as sonic_pi
from color import matlab

class VoiceSetup:
    __cancel_color = matlab['red']
    __done_color   = matlab['green']

    def __init__(self, rect, color, voice_num, hand_num_1, hand_num_2):
        self.visible     = False
        self.rect        = rect
        self.color       = color
        self.buttons     = {}
        self.labels      = []
        self.label_rects = []
        self.configs     = []

        btn_width = math.floor(1.5 * style.ctrl_btn_height)

        self.buttons['cancel'] = button.Button( \
            pg.Rect(rect.right - 2*btn_width, rect.top, \
                    btn_width, style.ctrl_btn_height), \
            self.__cancel_color, text=style.ctrl_text_cancel, \
            text_color=style.color_text, text_font=style.font_symb)

        self.buttons['done'] = button.Button( \
            pg.Rect(rect.right - btn_width, rect.top, \
                    btn_width, style.ctrl_btn_height), \
            self.__done_color, text=style.ctrl_text_done, \
            text_color=style.color_text, text_font=style.font_symb)

        title           = "Voice {} Setup".format(voice_num)
        max_width       = rect.width  - 2*btn_width - 2*style.padding
        max_height      = style.ctrl_btn_height - 2
        title_font, _   = util.get_fitted_font(style.font_text, title, [max_width, max_height])
        self.title      = title_font.render(title, True, style.color_text)
        self.title_rect = self.title.get_rect()
        self.title_rect.midleft = (rect.left + style.padding, \
            rect.top + style.ctrl_btn_height // 2)

        num_voice   = 1
        num_effects = 3 if hand_num_2 == None else 6
        num_config  = num_voice + num_effects

        config_rects = util.make_rect_grid( \
            pg.Rect(rect.left, rect.top + style.ctrl_btn_height, \
                    rect.width, rect.height-style.ctrl_btn_height), \
            num_config, 1)

        label_height  = min(max_height, config_rects[0].height - 2)
        label_font, _ = util.get_fitted_font(style.font_text, title, [math.inf, label_height])
        add_label     = lambda text : self.labels.append(label_font.render(text, True, style.color_text))

        add_label("Voice")
        if hand_num_2 == None:
            add_label("X")
            add_label("Y")
            add_label("Z")
        else:
            add_label("X{}".format(hand_num_1))
            add_label("Y{}".format(hand_num_1))
            add_label("Z{}".format(hand_num_1))
            add_label("X{}".format(hand_num_2))
            add_label("Y{}".format(hand_num_2))
            add_label("Z{}".format(hand_num_2))

        max_width = 0
        for label in self.labels:
            width = label.get_rect().width
            if width > max_width:
                max_width = width

        voice_init       = sonic_pi.synths[0]
        effects_init     = sonic_pi.voice_map[voice_init].copy()
        num_effects_init = len(effects_init)
        # Always give the option of a particular axis mapping to no effect
        effects_init.append(sonic_pi.no_effect)

        for i in range(num_config):
            self.label_rects.append(self.labels[i].get_rect())
            self.label_rects[i].midleft = config_rects[i].midleft
            self.label_rects[i].move_ip(style.padding, 0)
            left_shift = 2*style.padding + max_width
            rect = pg.Rect(config_rects[i].left + left_shift, \
                config_rects[i].top, config_rects[i].width - left_shift, \
                config_rects[i].height)

            if i == 0:
                # Voice setup
                self.configs.append(scroll_option.ScrollOption(rect, \
                    voice_init, sonic_pi.synths))
                self.configs[-1].set_scroll_callback(self.__load_effects)
            else:
                # Effects setup
                if i-1 < num_effects_init:
                    effect = effects_init[i-1]
                else:
                    effect = sonic_pi.no_effect
                self.configs.append(scroll_option.ScrollOption(rect, \
                    effect, effects_init))
                self.configs[-1].set_scroll_callback(self.__check_effects)

        self.curr_voice   = self.get_voice()
        self.curr_effects = self.get_effects()

    def cleanup(self):
        for config in self.configs:
            config.cleanup()

    def register_event(self, event):
        if not self.visible:
            return
        for _, btn in self.buttons.items():
            btn.register_event(event)
        for config in self.configs:
            config.register_event(event)

    def set_visible(self, visible):
        self.visible = visible
        for config in self.configs:
            config.set_visible(visible)

    def get_visible(self):
        return self.visible

    def draw_on(self, surface):
        if not self.visible:
            return
        pg.draw.rect(surface, self.color, self.rect)
        surface.blit(self.title, self.title_rect)
        for i in range(len(self.labels)):
            surface.blit(self.labels[i], self.label_rects[i])
            self.configs[i].draw_on(surface)
        for _, btn in self.buttons.items():
            btn.draw_on(surface)

    def __restore_opts(self):
        # First restore voice setting
        self.configs[0].set_opt(self.curr_voice)
        # Ensure all effects corresponding to the voice are loaded
        self.__load_effects(self)
        # Now restore effects settings
        for i in range(1,len(self.configs)):
            self.configs[i].set_opt(self.curr_effects[i-1])

    def canceled(self):
        if self.visible and self.buttons['cancel'].is_activated():
            self.__restore_opts()
            return True
        return False

    def done(self):
        if self.visible and self.buttons['done'].is_activated():
            self.curr_voice   = self.get_voice()
            self.curr_effects = self.get_effects()
            return True
        return False

    def get_voice(self):
        return self.configs[0].get_opt()

    def get_effects(self):
        return [config.get_opt() for config in self.configs[1:]]

    def __load_effects(self, arg=None):
        voice = self.configs[0].get_opt()
        effects = sonic_pi.voice_map[voice].copy()
        effects.append(sonic_pi.no_effect)
        for i in range(1,len(self.configs)):
            effect = effects[i-1] if i-1 < len(effects) else sonic_pi.no_effect
            self.configs[i].set_opts(effect, effects)

    def __check_effects(self, arg=None):
        effects = self.get_effects()
        # Remove the 'no effect' option because it can occur multiple times
        effects = list(filter((sonic_pi.no_effect).__ne__, effects))
        at_least_one_effect = (len(effects) > 0)
        all_effects_unique  = (len(set(effects)) == len(effects))
        self.buttons['done'].set_enabled( \
            at_least_one_effect and all_effects_unique)
