import pygame as pg
import style

class CoordDisplay:
    def __init__(self, label, label_width, coord_min, coord_max, coord_init, font, rect):
        self.visible    = False
        self.min        = coord_min
        self.max        = coord_max
        self.label      = font.render(label, True, style.color_text)
        self.label_rect = self.label.get_rect()
        self.label_rect.midleft = rect.midleft
        self.graph_rect = pg.Rect(rect.left + label_width + style.padding, rect.top, \
                                  rect.width - label_width - style.padding, rect.height)
        self.fill_rect  = pg.Rect(self.graph_rect.left   + style.width_bar_outline, \
                                  self.graph_rect.top    + style.width_bar_outline, \
                                  self.graph_rect.width  - 2*style.width_bar_outline, \
                                  self.graph_rect.height - 2*style.width_bar_outline)
        self.label_rect.left = rect.left
        self.set(coord_init)

    def cleanup(self):
        pass

    def set(self, coord):
        self.coord = max(self.min, min(self.max, coord))

    def get(self):
        return self.coord

    def set_visible(self, visible):
        self.visible = visible

    def get_visible(self):
        return self.visible

    def draw_on(self, surface):
        if not self.visible:
            return
        fill_l = self.fill_rect.left + (0          - self.min) / (self.max - self.min) * self.fill_rect.width
        fill_r = self.fill_rect.left + (self.coord - self.min) / (self.max - self.min) * self.fill_rect.width
        if fill_l > fill_r:
            fill_l, fill_r = fill_r, fill_l
        fill_l = max(fill_l, self.fill_rect.left)
        fill_r = min(fill_r, self.fill_rect.right)
        fill_rect = pg.Rect(fill_l, self.fill_rect.top, \
                            fill_r-fill_l+1, self.fill_rect.height)

        surface.blit(self.label, self.label_rect)
        pg.draw.rect(surface, style.color_screen,      self.graph_rect)
        pg.draw.rect(surface, style.color_bar_outline, self.graph_rect, 1)
        pg.draw.rect(surface, style.color_bar_fill,    fill_rect)
