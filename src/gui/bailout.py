# bailout.py
#
# Provides bailout options for otherwise infinite graphics loops.
# Bailouts can be PiTFT button(s) and/or a loop timeout.
#
# Author: Aaron Logan (apl86)
# Date: 8 October 2020

import math, time
import RPi.GPIO as gpio

class Bailout:
    # Constructor: configures GPIO ports to listen to PiTFT buttons. Pressing
    # any of the buttons given by btn_chans causes __quit_callback() to run.
    def __init__(self, \
                 btn_chans=[17,22,23,27], \
                 sw_debounce_ms=300, \
                 timeout=math.inf):
        self.running    = True
        self.start_time = time.time_ns() * 1e-9
        self.timeout    = timeout
        self.btn_chans  = btn_chans
        gpio.setmode(gpio.BCM)
        for channel in self.btn_chans:
            gpio.setup(channel, gpio.IN, pull_up_down=gpio.PUD_UP)
            gpio.add_event_detect(channel, gpio.FALLING, \
                    callback=lambda x: self.__quit_callback(), \
                    bouncetime=sw_debounce_ms)

    def cleanup(self):
        gpio.cleanup(self.btn_chans)

    def __quit_callback(self):
        self.running = False

    # Function to be used as condition for infinite graphics loop
    def is_running(self):
        curr_time = time.time_ns() * 1e-9
        return self.running and curr_time - self.start_time < self.timeout

    # Sometimes external actions require a loop quit
    def set_running(self, running):
        self.running = running
