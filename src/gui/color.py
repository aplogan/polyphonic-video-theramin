# Author: Aaron Logan (apl86)
# Date: 9 October 2020

import math
import pygame as pg

matlab = { 'black'  : (  0,  0,  0),
           'blue'   : (  0,114,189),
           'orange' : (217, 83, 25),
           'yellow' : (237,177, 32),
           'purple' : (126, 47,142),
           'green'  : (119,172, 48),
           'cyan'   : ( 77,190,238),
           'red'    : (162, 20, 37),
           'white'  : (255,255,255) }

def darken(color, factor=0.5):
    if isinstance(color, pg.Color):
        r, g, b, a = color.r, color.g, color.b, color.a
    elif (isinstance(color, tuple) or isinstance(color, list)) \
        and len(color) == 3:
        [r, g, b], a = list(color), 255
    elif (isinstance(color, tuple) or isinstance(color, list)) \
        and len(color) == 4:
        r, g, b, a = list(color)
    else: # Some other type; no idea how to handle it
        return color
    return pg.Color(math.floor(r * factor), \
                    math.floor(g * factor), \
                    math.floor(b * factor), \
                    a)
